import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.junit.runner.RunWith;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@RunWith(Cucumber.class)
@CucumberOptions
        (
                features 	= {"src/main/resources/featurefiles"},
                glue		= {"src/main/java/stepdefinitions"},
                tags		={"@ApplyOnlineFunctionality"},
                monochrome	= true,
                plugin = {"com.cucumber.listener.ExtentCucumberFormatter:output/report.html"}
        )

public class TestRunner extends AbstractTestNGCucumberTests {

    @BeforeTest
    public static void setupTest()
    {
        InputStream input = null;
        Properties prop = new Properties();

        try
        {
            input = new FileInputStream("global.properties");

            // load a properties file
            prop.load(input);

        }
        catch (FileNotFoundException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e)
        {
            // TODO Auto-generated
            e.printStackTrace();
        }


    }

    @AfterTest
    public void tearDown() throws IOException  {
        System.out.println(" Ending test ");

    }

}
