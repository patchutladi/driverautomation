package stepdefinitions;

import common.ApplyOnlineImplementation;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefinitions {

    ApplyOnlineImplementation applyOnlineImplementation = new ApplyOnlineImplementation();

    @Given("^that i have a \"([^\"]*)\", and \"([^\"]*)\"$")
    public void that_i_have_a_and(String name, String emailAddress) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        //throw new PendingException();
        applyOnlineImplementation.enterNameAndEmailAddress(name,emailAddress);
    }

    @When("^I submit my details without uploading a file$")
    public void i_submit_my_details_without_uploading_a_file() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        applyOnlineImplementation.submitDetailsCaptured();
    }

    @Then("^an error message appears$")
    public void an_error_message_appears() throws Throwable {
        // Write code here that turns the phrase above into concrete actions

        applyOnlineImplementation.validateErrorMessage();
    }



}
