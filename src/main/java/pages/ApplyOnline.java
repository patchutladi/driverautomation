package pages;

public class ApplyOnline {
    public static String careers_link_xpath= "//*[@id=\"menu-item-1373\"]/a";
    public static String country_link_xpath="/html/body/section/div[2]/div/div/div/div[3]/div[2]/div/div/div[3]/div[2]/div/div/div[4]/a";
    public static String job_link_xpath= "/html/body/section/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/a[contains(text(),\"Interns\")]";
    public static String apply_online_btn_xpath="//*[@id=\"wpjb-scroll\"]/div[1]/a";
    public static String name_xpath="//*[@id=\"applicant_name\"]";
    public static String email_xpath="//*[@id=\"email\"]";
    public static String phone_xpath="//*[@id=\"phone\"]";
    public static String send_application_btn="//*[@id=\"wpjb_submit\"]";
    public static String error_submit_xpath="//*[@id=\"wpjb-apply-form\"]/fieldset[1]/div[5]/div/ul/li";
}
