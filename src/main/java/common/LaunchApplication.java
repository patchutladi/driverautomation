package common;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.Properties;

public class LaunchApplication {

    public static WebDriver wdriver;
    public static String strCapAppPackage = "";
    public static String strCapPlatformName = "";
    public static String strBuildBatFilePath = "";
    public static String Browser = "";
    static Properties prop = new Properties();

    public static void setupTest()
    {
        InputStream input = null;
        Properties prop = new Properties();

        try
        {
            input = new FileInputStream("global.properties");

            // load a properties file
            prop.load(input);

        }
        catch (FileNotFoundException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e)
        {
            // TODO Auto-generated
            e.printStackTrace();
        }


    }
    public void openILABWebsite() throws Throwable, MalformedURLException
    {
        setupTest();
        String appName="";

        //Browser = prop.getProperty("browser.name");
        Browser = "GC";
        appName = "https://www.ilabquality.com/";
        //strCapAppPathName = prop.getProperty("app");
        System.out.println(" Starting test - Opening browser");

        Thread.sleep(1000);

        // ********************************************this code is for Google chrome*******************************************
        if (Browser.equals("GC"))
        {
            System.out.println(" Chrome browser opened");
            DesiredCapabilities cap = DesiredCapabilities.chrome();
            cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
            System.setProperty("webdriver.chrome.driver", "./BrowserDrivers/chromedriver");
            wdriver = new ChromeDriver(cap);
            WebDriverUtilities util = new WebDriverUtilities(wdriver);
            wdriver.get(appName);
            Thread.sleep(3000);
            wdriver.manage().window().maximize();
            Thread.sleep(5000);
        }

        // ********************************************IE*******************************************
        if (Browser.equals("IE"))
        {

            DesiredCapabilities  cap = DesiredCapabilities.internetExplorer();
            cap.setCapability(CapabilityType.BROWSER_NAME, "internet explorer");
            cap.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
            cap.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);
            cap.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
            cap.setCapability("allow-blocked-content", true);
            cap.setCapability("allowBlockedContent", true);
            System.setProperty("webdriver.ie.driver", "./BrowserDrivers/IEDriverServer.exe");
            wdriver = new InternetExplorerDriver(cap);
            WebDriverUtilities util = new WebDriverUtilities(wdriver);
            wdriver.get(appName);
            wdriver.manage().window().maximize();
        }


    }
}
