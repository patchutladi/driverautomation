package common;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import jdk.nashorn.internal.runtime.regexp.joni.constants.StringType;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.ApplyOnline;

import static common.LaunchApplication.wdriver;

public class ApplyOnlineImplementation {


    public void launchApp() throws Throwable {
        LaunchApplication launchApplication = new LaunchApplication();
        launchApplication.openILABWebsite();


        clickMe(wdriver, ApplyOnline.careers_link_xpath);
        clickMe(wdriver,ApplyOnline.country_link_xpath);
        Thread.sleep(1200);
        clickMe(wdriver,ApplyOnline.job_link_xpath);
        clickMe(wdriver,ApplyOnline.apply_online_btn_xpath);
    }

    public  void enterNameAndEmailAddress(String name, String emailAddress) throws Throwable {
        launchApp();
        enterText(wdriver, ApplyOnline.name_xpath,name);
        enterText(wdriver, ApplyOnline.email_xpath,emailAddress);
        //Generate phone number
        String phoneNumber  = String.valueOf(Math.random()*100000 + 3333300000L);
        enterText(wdriver, ApplyOnline.phone_xpath, phoneNumber);
    }

    public void submitDetailsCaptured()
    {
        clickMe(wdriver, ApplyOnline.send_application_btn);
    }

    public boolean validateErrorMessage()
    {
        return getTextOnElement(wdriver, ApplyOnline.error_submit_xpath).
                contains("You need to upload at least one file");
    }

    public void clickMe(WebDriver driver,String elementXpath)
    {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 120);
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(elementXpath))));
            driver.findElement(By.xpath(elementXpath)).click();
        }
        catch (ElementNotFoundException errorMessage)
        {
            System.out.println(elementXpath + "  xpath not found + \n" + errorMessage.toString());
        }
    }

    public void enterText(WebDriver driver,String elementXpath, String text)
    {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 120);
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(elementXpath))));
            driver.findElement(By.xpath(elementXpath)).sendKeys(text);
        }
        catch (ElementNotFoundException errorMessage)
        {
            System.out.println(elementXpath + "  xpath not found + \n" + errorMessage.toString());
        }
    }

    public String getTextOnElement(WebDriver driver,String elementXpath)
    {
        String returnedText = "";
        try {
            WebDriverWait wait = new WebDriverWait(driver, 120);
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(elementXpath))));
            returnedText = driver.findElement(By.xpath(elementXpath)).getText();
        }
        catch (ElementNotFoundException errorMessage)
        {
            System.out.println(elementXpath + "  xpath not found + \n" + errorMessage.toString());
        }
        return returnedText;
    }


}
