@ApplyOnlineFunctionality
Feature: Apply Online For South Africa
  As a job seeker I want to apply for a job at ILAB South Africa


  Scenario Outline: Apply online for any job opening
    Given that i have a "<name>", and "<emailaddress>"
    When I submit my details without uploading a file
    Then an error message appears

    Examples:
      | name          | emailaddress                         |
      | Test Quality  | automationAssessment@iLABQuality.com |
      #| Test Quality2 | automationAssessment@iLABQuality.com |
     # | Test Quality4 | automationAssessment@iLABQuality.com |

